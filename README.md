Công Ty CP Kinh Doanh Và Phát Triển Địa Ốc Vietstarland (VSL) là đại lý chiến lược và phân phối số 1 biệt thự và căn hộ chung cư dòng bất động sản thương hiệu Vinhomes.
Với tư cách là đại lý phân phối số 1 các dự án bất động sản Vinhomes, Vietstarland đang là đại lý chiến lược có quỹ hàng tốt nhất tại những dự án “đình đám” trên thị trường như: Vinhomes Riverside, Vinhomes Riverside – The Harmony , Vinhomes Thăng Long, Vinhomes Metropolis, D.Le Roi Soleil, D’. Palais de Louis, Vinhomes Green Bay, … và các dự án nghỉ dưỡng thương hiệu Vinpearl Resort & Villas tại các bãi biển đẹp nhất Việt Nam như: Vinpearl Long Beach Villas, Vinpearl Empire Condotel, Vinpearl Riverfont Condotel, Vinpearl Beachfront Condotel, Vinpearl Luxury Villas…
Địa chỉ: TTGD BĐS Vinhomes, số 10 đường Tương Lai, hầm B1, Times City, Phố Minh Khai, Vĩnh Tuy, Hai Bà Trưng, Hà Nội
Email: vietstarland.invest@gmail.com
Điện thoại: 096 466 88 88
Website: https://vietstarland.vn/